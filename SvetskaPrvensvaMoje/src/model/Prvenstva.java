package model;

public class Prvenstva {

	protected int godina;
	protected Drzava domacin;
	protected Drzava osvajac;
	public Prvenstva(int godina, Drzava domacin, Drzava osvajac) {
		super();
		this.godina = godina;
		this.domacin = domacin;
		this.osvajac = osvajac;
	}
	public Prvenstva() {
		super();
	}
	public int getGodina() {
		return godina;
	}
	public void setGodina(int godina) {
		this.godina = godina;
	}
	public Drzava getDomacin() {
		return domacin;
	}
	public void setDomacin(Drzava domacin) {
		this.domacin = domacin;
	}
	public Drzava getOsvajac() {
		return osvajac;
	}
	public void setOsvajac(Drzava osvajac) {
		this.osvajac = osvajac;
	}
	@Override
	public String toString() {
		return "Prvenstva [godina=" + godina + ", domacin=" + domacin
				+ ", osvajac=" + osvajac + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}//kraj

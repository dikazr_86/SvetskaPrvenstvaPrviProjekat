package ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import utils.ScannerWrapper;
import dao.PrvenstvaDAO;

public class ApplicationUI {
	public static Connection conn;
	public static PrvenstvaDAO prDAO = new PrvenstvaDAO();
	
	static {
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");

			// konekcija
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/sys", "jwd11_3", "jwd11_3");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
//meni osnovne opcije	
	public static void main(String[] args)  {
		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				PrvenstvaUI.ispisiSvaPrvenstva();
				break;
			case 2:
				DrzavaUI.ispisiSveDrzave();
				break;
			case 3:
				System.out.println(PrvenstvaUI.pronadjiPr());
				break;
			case 4:
				PrvenstvaUI.unosNovogPrvenstva();
				break;
			case 5:
				PrvenstvaUI.brisanjePodatakaOprvenstvu();
				break;
			/*case 6:
				ManifestacijaUI.brisanjePodatakaOManifestaciji();
				break;*/
			case 6:
				DrzavaUI.brisanjePodatakaODrzavi();
				break;
			case 7:
				DrzavaUI.izmenaPodatakaODrzavi();
				break;
			case 8:
				DrzavaUI.unosDrzave();
				//PrvenstvaUI.citanjeIzFajla();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}	
// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Manifestacije Srbije - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Prikaz svih prvenstava");
		System.out.println("\tOpcija broj 2 - Prikaz svih drzava");
		System.out.println("\tOpcija broj 3 - Pretraga prvenstava po godini ");
		System.out.println("\tOpcija broj 4 - Unos prvenstva");
		//System.out.println("\tOpcija broj 5 - Izmena manifestacije");
		System.out.println("\tOpcija broj 5 - Brisanje prvenstva");
		System.out.println("\tOpcija broj 6 - Brisanje drzave");
		System.out.println("\tOpcija broj 7 - Izmena drzave");
		System.out.println("\tOpcija broj 8 - Unos drzave");
		
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}
}

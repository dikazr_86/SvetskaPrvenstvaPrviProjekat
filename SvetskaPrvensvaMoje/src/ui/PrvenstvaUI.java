package ui;

import java.util.List;

import utils.ScannerWrapper;
import model.Prvenstva;
import model.Drzava;
import dao.PrvenstvaDAO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import utils.ScannerWrapper;

public class PrvenstvaUI {

	public static PrvenstvaDAO prDAO = new PrvenstvaDAO();	
	
	
// ispisi sva prvenstva
		public static void ispisiSvaPrvenstva() {
			List<Prvenstva> svaPrvenstva = prDAO.getAll(ApplicationUI.conn);
				
			for (Prvenstva p : svaPrvenstva) {
				System.out.println(p);
			}
		}	
// pronadji prvenstvo
		public static Prvenstva pronadjiPr(){
			Prvenstva retVal = null;
		    System.out.print("Unesi godinu prvenstva:");
			int pr_godina = ScannerWrapper.ocitajCeoBroj();
			retVal = pronadjiPr (pr_godina);
					if (retVal == null){
						System.out.println("***Prvenstvo SA unetom godinnom NE POSTOJI U EVIDENCIJI.***");
					}
					return retVal;
				}
	
// pronadji prvenstvo
public static Prvenstva pronadjiPr(int godina) {
	Prvenstva retVal = prDAO.getPrvenstvaByYear(ApplicationUI.conn, godina);
	return retVal;
	}	

//unos novog prvenstva.
	public static void unosNovogPrvenstva() {
		int godina = -1;
		System.out.print("Unesi godinu prvenstva:");
		int pr_godina = ScannerWrapper.ocitajCeoBroj();
		System.out.print("Unesi drzavu domacina:");
		Drzava domacin = DrzavaUI.pronadjiDrzavu(ScannerWrapper.ocitajCeoBroj());
		System.out.print("Unesi drzavu pobednicu:");
		Drzava osvajac =DrzavaUI.pronadjiDrzavu(ScannerWrapper.ocitajCeoBroj());
		//Drzava drzava = DrzavaUI.pronadjiDrzavu();
			
		if(domacin == null){
			System.out.println("***PRVENSTVO SA UNETOM GODINOM NE POSTOJI U EVIDENCIJI.***");
			return;
		}

		Prvenstva p = new Prvenstva(pr_godina, domacin, osvajac);
		PrvenstvaUI.prDAO.add(ApplicationUI.conn, p);
		System.out.println("***USPESAN UNOS***");
	}
//brisanje prvenstva
	public static void brisanjePodatakaOprvenstvu(){
		Prvenstva p = pronadjiPr();
		if(p != null){
			PrvenstvaUI.prDAO.delete(ApplicationUI.conn, p.getGodina());
			System.out.println("***USPESNO BRISANJE***");
		}
	}	
	//upis u fajl	
	/*	public static void upisUFajl(){		
			try {
				String tekst = prDAO.maxPrvenstva(ApplicationUI.conn);
				
				File file = new File("data\\manifestacije.csv");
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}

				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(tekst);
				bw.close();

			} catch (IOException e) {
				e.printStackTrace();
			}		
		}
		*/
	
	//citanje iz fajla
/*		public static void citanjeIzFajla(){
					
			try (BufferedReader br = new BufferedReader(new FileReader("data\\manifestacije.csv")))
			{
				String sCurrentLine;
				while ((sCurrentLine = br.readLine()) != null) {
					System.out.println(sCurrentLine);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} 		
		}	
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}//kraj

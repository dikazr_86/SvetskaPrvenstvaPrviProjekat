package ui;

import java.util.List;

import model.Drzava;
import dao.DrzavaDAO;
import utils.ScannerWrapper;

public class DrzavaUI {
	public static DrzavaDAO drzavaDAO = new DrzavaDAO();
	// ispisi sve drzavu
		public static void ispisiSveDrzave() {
			List<Drzava> sveDrzave = drzavaDAO.getAll(ApplicationUI.conn);			
			for (Drzava a : sveDrzave) {
				System.out.println(a);
				}
		}	

		// pronadji drzavu
		public static Drzava pronadjiDrzavu() {
			Drzava retVal = null;
			System.out.print("Unesi id drzave:");
			int v_id = ScannerWrapper.ocitajCeoBroj();
			retVal = pronadjiDrzavu(v_id);
			
			if (retVal == null){
				System.out.println("***DRZAVA SA UNETIM ID NE POSTOJI U EVIDENCIJI.***");
			}
			return retVal;
		}
// pronadji drzavu
		public static Drzava pronadjiDrzavu(int v_id) {
			Drzava retVal = drzavaDAO.getDrzavaById(ApplicationUI.conn, v_id);
			return retVal;
		}	
/* public static Drzava pronadjiDrzavu(int id){
	Drzava retVal = drzavaDAO.getDrzavaById(ApplicationUI.conn, id);
	return retVal;
	}
*/			
	// izmena , menja se samo naziv
	/*	public static void izmenaPodatakaODrzavi() {
			Drzava drzava = pronadjiDrzavu();
			if(drzava != null){
				System.out.print("Unesi novi naziv drzave :");
				String noviNaziv = ScannerWrapper.ocitajTekst();
				drzava.setNaziv(noviNaziv);
					
				DrzavaUI.drzavaDAO.update(ApplicationUI.conn, drzava);
					if(DrzavaUI.drzavaDAO.update(ApplicationUI.conn, drzava) == true){
						System.out.println("***USPESNA IZMENA***");
					}else{
						System.out.println("***GRESKA***");
					}
			} 
		}
		*/
// izmena grada, menja se samo naziv
public static void izmenaPodatakaODrzavi() {
	Drzava drzava = pronadjiDrzavu();
			if(drzava != null){
				System.out.print("Unesi novi naziv drzave :");
				String noviNaziv = ScannerWrapper.ocitajTekst();
				drzava.setNaziv(noviNaziv);
					
				DrzavaUI.drzavaDAO.update(ApplicationUI.conn, drzava);
					if(DrzavaUI.drzavaDAO.update(ApplicationUI.conn, drzava) == true){
						System.out.println("***USPESNA IZMENA***");
					}else{
						System.out.println("***GRESKA***");
					}
			} 
		}		
	//brisanje drzave
		public static void brisanjePodatakaODrzavi(){
			Drzava drzava = pronadjiDrzavu();
			if(drzava != null){
				DrzavaUI.drzavaDAO.delete(ApplicationUI.conn, drzava.getId());
				System.out.println("***USPESNO BRISANJE***");
			}
		}	
//unos drzave
public static void unosDrzave() {
			System.out.print("Molimo unesite naziv drzave: ");
			String naziv = ScannerWrapper.ocitajTekst();
			Drzava d = new Drzava();
			d.setNaziv(naziv);
			boolean uspeo = drzavaDAO.add(ApplicationUI.conn, d);
			if (uspeo)
				System.out.println("Uspesno je dodana drzava");
			else 
				System.out.println("Nije uspelo dodavanje.");
		}
















}//kraj

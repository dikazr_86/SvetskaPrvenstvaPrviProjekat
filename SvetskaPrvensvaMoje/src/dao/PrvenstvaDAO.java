package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import model.Prvenstva;
import model.Drzava;


public class PrvenstvaDAO {
	public List<Prvenstva> getAll(Connection conn) {
		List<Prvenstva> retVal = new ArrayList<Prvenstva>();
		try {
			String query = "SELECT p.godina, p.drzava_domacin, " +
					"p.drzava_pobednica, d1.naziv, d2.naziv " +
					"FROM prvenstvo p " +
					"left join drzava d1 on " +
					"p.drzava_domacin = d1.id " + 
					"left join drzava d2 on " +
					"p.drzava_pobednica = d2.id";
			
			/* BEZ JOIN
			String query = "SELECT godina, drzava_domacin, drzava_pobednica "
					+ "from prvenstva";
			*/
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query);
			while (rset.next()) {
				int godina = rset.getInt(1);
				int idDomacina = rset.getInt(2);
				int idOsvajaca = rset.getInt(3);
				String nazivDomacina = rset.getString(4);
				String nazivOsvajaca = rset.getString(5);
				
				/* VARIJANTA BEZ JOIN 
				DrzavaDao drzavaDao = new DrzavaDao();				
				Drzava drzavaDomacin = 
						drzavaDao.getDrzavaById(conn, idDomacina);
				Drzava drzavaOsvajac = drzavaDao.getDrzavaById(conn, idOsvajaca);
				
				*/				
			
				Prvenstva p = new Prvenstva();
				p.setGodina(godina);
				//ako smo uradili JOIN, u istom upitu smo preuzeli i naziv drzave domacina
				Drzava domacin = new Drzava(nazivDomacina, idDomacina);
				p.setDomacin(domacin);
				
				Drzava osvajac = new Drzava(nazivOsvajaca,idOsvajaca);
				p.setOsvajac(osvajac);
				
				retVal.add(p);
			}
			rset.close();
			stmt.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retVal;
	}
	
//pronadji prvenstvo po gradu	
		public Prvenstva getPrvenstvaByYear(Connection conn, int godina) {
			Prvenstva p = null;
			
			try {
				Statement stmt = conn.createStatement();
				ResultSet rset = stmt.executeQuery("select p.godina from prvenstvo p where godina=" + godina);

				if (rset.next()) {
					int v_godina = rset.getInt(1);
				    
					 p = new Prvenstva(v_godina,null,null);
				}
				rset.close();
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return p;
		}	
//unos novog prvenstva
/*		public boolean add(Connection conn, Prvenstva p){
			boolean retVal = false;
			try {
				String update = "insert into prvenstvo (godina, drzava_domacin,drzava_pobednica ) values (?, ?, ?)";
				PreparedStatement pstmt = conn.prepareStatement(update);
				pstmt.setInt(1, p.getGodina());
				//pstmt.setInt(2, p.getDomacin());
				//pstmt.setInt(3, p.getDrzava().getOsvajac());
					
				if(pstmt.executeUpdate() == 1){
					retVal = true;
				}
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}
*
*/
//unos Prvenstva
public boolean add(Connection conn, Prvenstva p){
			boolean retVal = false;
			try {
				String update = "insert into prvenstvo (godina, drzava_domacin,drzava_pobednica) values (?, ?, ?)";
				PreparedStatement pstmt = conn.prepareStatement(update);
				pstmt.setInt(1, p.getGodina());
				pstmt.setInt(2, p.getDomacin().getId());
				pstmt.setInt(3, p.getOsvajac().getId());
					
				if(pstmt.executeUpdate() == 1){
					retVal = true;
				}
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}
//brisanje prvenstva	
		public boolean delete(Connection conn, int godina) {
			boolean retVal = false;
			try {
				String update = "DELETE FROM prvenstvo WHERE godina = " + godina;
				Statement stmt = conn.createStatement();
				if (stmt.executeUpdate(update) == 1)
					retVal = true;
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}
}//kraj

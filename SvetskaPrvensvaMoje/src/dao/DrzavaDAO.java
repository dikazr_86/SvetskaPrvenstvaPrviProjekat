package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Drzava;


public class DrzavaDAO {

//pronadji drzavu po id
	/*public Drzava getDrzavaById(Connection conn, int id) {
		Drzava retVal = null;
		try {		
			String query = "SELECT naziv FROM drzava where id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet rset = stmt.executeQuery();
			if (rset.next()) {
				String naziv = rset.getString(1);				
				retVal = new Drzava(naziv,id);				
			}
			rset.close();
			stmt.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
		return retVal;
	}
*/
	public Drzava getDrzavaById(Connection conn, int id) {
		Drzava drzava = null;
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery("select id, naziv from drzava  where id=" + id);

			if (rset.next()) {
				int v_id = rset.getInt(1);
				String naziv = rset.getString(2);
				
				drzava = new Drzava(naziv, v_id);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return drzava;
	}
	//lista svih drzava
	public List<Drzava> getAll(Connection conn) {
		List<Drzava> retVal = new ArrayList<Drzava>();
		try {
			String query = "select id, naziv from drzava";
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt.executeQuery(query.toString());
			
			while (rset.next()) {
				int id = rset.getInt(1);
				String naziv = rset.getString(2);
				
				Drzava drzava = new Drzava(naziv, id);
				retVal.add(drzava);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}
//izmena naziva drzave	
	public boolean update(Connection conn, Drzava drzava) {
		boolean retVal = false;
		try {
			String update = "UPDATE drzava SET naziv=? WHERE id =?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, drzava.getNaziv());
			pstmt.setInt(2, drzava.getId());
			
		if(pstmt.executeUpdate() == 1)
			retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}	
	//brisanje drzave	
		public boolean delete(Connection conn, int id) {
			boolean retVal = false;
			try {
				String update = "DELETE FROM drzava WHERE id = " + id;
				Statement stmt = conn.createStatement();
				if (stmt.executeUpdate(update) == 1)
					retVal = true;
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}	
	
//unos drzave
public boolean add(Connection conn, Drzava novaDrzava){
			boolean retVal = false;
			try {
				String update = "INSERT INTO drzava (naziv) values (?)";
				PreparedStatement pstmt = conn.prepareStatement(update);
				pstmt.setString(1, novaDrzava.getNaziv());
				int brojSlogova = pstmt.executeUpdate();
				if (brojSlogova == 1){
					retVal = true;
					int dobijeniId = getInsertedId(conn);
					novaDrzava.setId(dobijeniId);
				}
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return retVal;
		}
private int getInsertedId(Connection conn) {
	// TODO Auto-generated method stub
	return 0;
}		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}//kraj
